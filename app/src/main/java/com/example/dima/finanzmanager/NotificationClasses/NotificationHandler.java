package com.example.dima.finanzmanager.NotificationClasses;


import android.annotation.TargetApi;
import android.app.AlarmManager;

import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.R;
import com.example.dima.finanzmanager.classes.Transaction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Dima on 17.12.2017.
 */

public class NotificationHandler {

    private static final String LOG_TAG = NotificationHandler.class.getSimpleName();

    private static Calendar time;
    private static Calendar transactionsTimer;

    @TargetApi(19)
    public void createTimedTransactions(Context context, int accountID) {
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, TransactionsReceiver.class);
        intent.putExtra(DBHelper.ACCOUNT_ID, accountID);
        PendingIntent pending = PendingIntent.getBroadcast(context, accountID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        setTransactionsTimer();
        mgr.set(AlarmManager.RTC_WAKEUP, transactionsTimer.getTimeInMillis(), pending);
    }


    public void createTimedNotification(Context context, int accountID) {
            AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra(DBHelper.ACCOUNT_ID, accountID);
            PendingIntent pending = PendingIntent.getBroadcast(context, accountID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            setTimedNotificationTime();
            mgr.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pending);
    }

    public static void setTimedNotificationTime() {
            time = Calendar.getInstance();
            time.add(Calendar.DAY_OF_MONTH, 1);
            int currentYear = time.get(Calendar.YEAR);
            int currentMonth = time.get(Calendar.MONTH);
            int nextDay = time.get(Calendar.DAY_OF_MONTH);
            time.clear();
            time.set(currentYear, currentMonth, nextDay, 8, 00);
            if (time.getTimeInMillis() < System.currentTimeMillis()) {
                time.add(Calendar.DAY_OF_MONTH, 1);
            }
    }

    public static void setTransactionsTimer() {
        transactionsTimer = Calendar.getInstance();
        int currentYear = transactionsTimer.get(Calendar.YEAR);
        int currentMonth = transactionsTimer.get(Calendar.MONTH);
        int lastDay = transactionsTimer.getActualMaximum(Calendar.DAY_OF_MONTH);
        transactionsTimer.clear();
        transactionsTimer.set(currentYear, currentMonth, lastDay, 23, 00);
        if (transactionsTimer.getTimeInMillis() < System.currentTimeMillis()) {
            transactionsTimer.add(Calendar.DAY_OF_MONTH, 1);
            lastDay = transactionsTimer.getActualMaximum(Calendar.DAY_OF_MONTH);
            transactionsTimer.set(Calendar.DAY_OF_MONTH, lastDay);
            Log.d(LOG_TAG, "Letzter Tag diesen Monats: " + transactionsTimer.getTime());
        }
    }
}
