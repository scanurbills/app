package com.example.dima.finanzmanager.NotificationClasses;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.support.v4.app.NotificationCompat;
import android.view.SoundEffectConstants;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NavigatorActivity;
import com.example.dima.finanzmanager.R;
import com.example.dima.finanzmanager.ScanOrOverview;
import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Transaction;

import java.util.List;

/**
 * Created by Dima on 25.01.2018.
 */

public class TransactionsReceiver extends BroadcastReceiver {

    public static final String LOG_TAG = TransactionsReceiver.class.getSimpleName();

    private static int accountID;

    //Datenbankschnittstelle
    private DataSource src;


    @Override
    public void onReceive(Context context, Intent intent) {



        accountID = intent.getExtras().getInt(DBHelper.ACCOUNT_ID);

        src = new DataSource(context);

        src.open();

        Account account = src.getAccount(accountID);
        List<Transaction> transactionsList = src.getAllTransactions(accountID);

        Transaction tmp;
        int amountOfMoneySum = 0;
        for (int count = 0; count < transactionsList.size(); count++) {
            tmp = transactionsList.get(count);
            amountOfMoneySum += tmp.getAmountOfMoneyInCents();
        }

        account.setAccountBalance(account.getAccountBalanceInCents() - amountOfMoneySum);
        src.changeAccount(accountID, account.getContentValues());

        src.close();

        NotificationHandler.setTransactionsTimer();
        new NotificationHandler().createTimedTransactions(context, accountID);

        Intent intent1 = new Intent(context, ScanOrOverview.class);
        intent1.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
        PendingIntent pending = PendingIntent.getActivity(context,
                accountID,
                intent1,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(account.getName())
                .setSmallIcon(R.drawable.ic_euro_notification)
                .setContentText("Transaktionen erledigt.")
                .setContentIntent(pending)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(0, builder.build());
    }
}
