package com.example.dima.finanzmanager.classes;

import android.content.ContentValues;

import com.example.dima.finanzmanager.DBClasses.DBHelper;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Dima on 23.01.2018.
 */

public class Transaction {

    private int id;
    private String name;
    private int accountID;
    private int amountOfMoneyInCents;
    private int type;

    public Transaction(int accountID, String name, int amountOfMoneyInCents, int type) {
        this.accountID = accountID;
        this.name = name;
        this.amountOfMoneyInCents = amountOfMoneyInCents;
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmountOfMoneyInCents(int amountOfMoneyInCents) {
        this.amountOfMoneyInCents = amountOfMoneyInCents;
    }

    public int getAccountID() {
        return accountID;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getAmountOfMoneyInCents() {
        return amountOfMoneyInCents;
    }

    public double getAmountOfMoneyInEuro() {
        double amountOfMoney = (double) amountOfMoneyInCents;
        return amountOfMoney / 100.0;
    }

    public String getAmountOfMoneyAsString() {
        return NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(getAmountOfMoneyInEuro());
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.TRANSACTION_NAME, name);
        cv.put(DBHelper.TRANSACTION_ACCOUNT_ID, accountID);
        cv.put(DBHelper.TRANSACTION_AMOUNT_OF_MONEY, amountOfMoneyInCents);
        cv.put(DBHelper.TRANSACTION_TYPE, type);
        return cv;
    }


}
