package com.example.dima.finanzmanager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Transaction;

/**
 * Created by Dima on 24.01.2018.
 */

class TransactionAdapter extends ArrayAdapter<Transaction> {


    public TransactionAdapter(Context context, int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.account_row, null);
        }

        Transaction transaction = getItem(position);

        TextView name = (TextView) v.findViewById(R.id.nameAccountRow);
        TextView balance = (TextView) v.findViewById(R.id.balanceAccountRow);
        name.setText(transaction.getName());
        balance.setText(transaction.getAmountOfMoneyAsString());

        return v;

    }

}
