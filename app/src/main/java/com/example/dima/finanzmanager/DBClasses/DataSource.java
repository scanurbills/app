package com.example.dima.finanzmanager.DBClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Receipt;
import com.example.dima.finanzmanager.classes.Transaction;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 14.12.2017.
 * Diese Klasse ist dafür zuständig die Datenbankverbindung aufrecht zu erhalten.
 * Sie ermöglicht das Hinzufügen, Lesen und Löschen von Datensätzen.
 * Auch wandelt sie Datensätze in Java-Objekte um.
 */

public class DataSource {

    private static final String LOG_TAG = DataSource.class.getSimpleName();

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public DataSource(Context context) {
        Log.d(LOG_TAG, "Unsere Datasource erzeugt den DBHelper.");
        dbHelper = new DBHelper(context);
    }



    //BASIC DATENBANKFUNKTIONEN.

    public void open() {
        Log.d(LOG_TAG, "Eine Referenz auf die Datenbank wird jetzt angefragt.");
        database = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "Datenbank-Referenz erhalten.");
    }

    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Datenbank mit Hilfe des DbHelpers geschlossen.");
    }

    private long insert(String table, ContentValues values) {
        long insertId = database.insert(table, null, values);
        Log.d(LOG_TAG, "Insert hat funktioniert.");
        return insertId;
    }

    public Cursor query(String table, String[] columns, String column_id, long insertId) {
        Cursor cursor = database.query(table, columns, column_id + "=" + insertId, null, null, null, null);
        return cursor;
    }



    //ACCOUNT-DATENBANKFUKNTIONEN (CREATE, READ, UPDATE, DELETE)

    public Account createAccount(String name, int accountBalanceInCents) {

            Account account = new Account(name, accountBalanceInCents);
            ContentValues values = account.getContentValues();
            long insertID = database.insert(DBHelper.TABLE_ACCOUNT, null, values);


            Account checkDB = getAccount((int) insertID);
            checkDB.setId((int) insertID);

            return checkDB;
    }

    public int deleteAccount(int id) {
        int result = database.delete(DBHelper.TABLE_ACCOUNT, DBHelper.ACCOUNT_ID + " = " + id, null);
        return result;
    }

    public Account getAccount(int accountID) {
        Cursor cursor = query(DBHelper.TABLE_ACCOUNT, DBHelper.COLUMNS_ACCOUNT, DBHelper.ACCOUNT_ID, accountID);
        cursor.moveToFirst();
        Account account = null;
        if (cursor.getCount() != 0)
            account = cursorToAccount(cursor);
        return account;
    }

    public List<Account> getAllAccounts() {


        List<Account> accountList = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.TABLE_ACCOUNT, DBHelper.COLUMNS_ACCOUNT, null, null, null, null, null);

            cursor.moveToFirst();
            for (int count = 1; count <= cursor.getCount(); cursor.moveToPosition(count++))
                accountList.add(cursorToAccount(cursor));

        cursor.close();

        return accountList;
    }

    public int changeAccount(int id, ContentValues cv) {
        int resultCode = database.update(DBHelper.TABLE_ACCOUNT, cv, "id =" + id, null);
        return resultCode;
    }

    private Account cursorToAccount(Cursor cursor) {

        int idID = cursor.getColumnIndex(DBHelper.ACCOUNT_ID);
        int nameID = cursor.getColumnIndex(DBHelper.ACCOUNT_NAME);
        int accountBalanceInCentsID = cursor.getColumnIndex(DBHelper.ACCOUNT_BALANCE);


        int id = cursor.getInt(idID);
        String name = cursor.getString(nameID);
        int accountBalanceInCents = cursor.getInt(accountBalanceInCentsID);


        Account account = new Account(name, accountBalanceInCents);
        account.setId(id);
        return account;
    }



    //RECEIPT-DATENBANKFUNKTIONEN (CREATE, READ, UPDATE, DELETE)

    public Receipt createReceipt(int accountID, byte[] image, int type) {
        Receipt receipt = new Receipt(image, accountID, type);
        ContentValues values = receipt.getContentValues();

        long insertID = insert(DBHelper.TABLE_RECEIPT, values);

        Receipt checkDB = getReceipt((int) insertID);

        return checkDB;
    }

    public int deleteReceipt(int receiptID) {
        int result = database.delete(DBHelper.TABLE_RECEIPT, DBHelper.RECEIPT_ID + " = " + receiptID, null);
        return result;
    }

    public Receipt getReceipt(int receiptID) {
        Cursor cursor = query(DBHelper.TABLE_RECEIPT, DBHelper.COLUMNS_RECEIPT, DBHelper.RECEIPT_ID, receiptID);
        cursor.moveToFirst();
        Receipt receipt = cursorToReceipt(cursor);
        cursor.close();
        return receipt;
    }

    public List<Receipt> getAllReceipts(int accountID) {

        List<Receipt> receiptList = new ArrayList<>();

        Cursor cursor = query(DBHelper.TABLE_RECEIPT, DBHelper.COLUMNS_RECEIPT, DBHelper.RECEIPT_ACCOUNT_ID, accountID);

        Log.d(LOG_TAG, "Gesucht Account-ID: " + accountID);
        Log.d(LOG_TAG, "Receipt Anzahl: " + cursor.getCount());

        Receipt tmp;
        for (int count = 0; count < cursor.getCount(); count++) {
            cursor.moveToPosition(count);
            tmp = cursorToReceipt(cursor);
            receiptList.add(tmp);
        }

        Log.d(LOG_TAG, "Anzahl der Elemente in receiptList: " + receiptList.size());

        cursor.close();

        return receiptList;

    }

    public int changeReceipt(int receiptID, int bruttoInCents) {
        Cursor cursor = query(DBHelper.TABLE_RECEIPT, DBHelper.COLUMNS_RECEIPT, DBHelper.RECEIPT_ID, receiptID);
        cursor.moveToFirst();
        Log.d(LOG_TAG, "cursorIsNullIn Column: " + cursor.isNull(cursor.getColumnIndex("id")));
        Log.d(LOG_TAG, "cursorCount: " + cursor.getCount());
        Receipt newReceipt = cursorToReceipt(cursor);
        cursor.close();
        //Account-Daten anpassen.
        Cursor accountCursor = query(DBHelper.TABLE_ACCOUNT, DBHelper.COLUMNS_ACCOUNT, DBHelper.ACCOUNT_ID, newReceipt.getAccountID());
        accountCursor.moveToFirst();
        Account newAccount = cursorToAccount(accountCursor);
        if (newReceipt.getType() == 1)
            newAccount.setAccountBalance(newAccount.getAccountBalanceInCents() + bruttoInCents - newReceipt.getBruttoInCents());
        else if (newReceipt.getType() == 0)
        newAccount.setAccountBalance(newAccount.getAccountBalanceInCents() - bruttoInCents + newReceipt.getBruttoInCents());
        changeAccount(newAccount.getId(), newAccount.getContentValues());
        accountCursor.close();
        //Ende.
                Log.d(LOG_TAG, "Alles values von oldReceipt: " + newReceipt.getId() + " " + newReceipt.getAccountID());
        newReceipt.setBruttoInCents(bruttoInCents);
        newReceipt.setMwStInCents();
        ContentValues cv = newReceipt.getContentValues();
        int resultCode = database.update(DBHelper.TABLE_RECEIPT, cv,"id = ?",new String[]{String.valueOf(receiptID)});
        return resultCode;
    }

    private Receipt cursorToReceipt(Cursor cursor) {

        int idID = cursor.getColumnIndex(DBHelper.RECEIPT_ID);
        int dateAddedID = cursor.getColumnIndex(DBHelper.RECEIPT_DATE_ADDED);
        int nettoWorthID = cursor.getColumnIndex(DBHelper.RECEIPT_NETTO);
        int bruttoWorthID = cursor.getColumnIndex(DBHelper.RECEIPT_BRUTTO);
        int MwStID = cursor.getColumnIndex(DBHelper.RECEIPT_MWST);
        int imageID = cursor.getColumnIndex(DBHelper.RECEIPT_IMAGE);
        int typeID = cursor.getColumnIndex(DBHelper.RECEIPT_TYPE);
        int accountidID = cursor.getColumnIndex(DBHelper.RECEIPT_ACCOUNT_ID);


        int id = cursor.getInt(idID);
        String dateAdded = cursor.getString(dateAddedID);
        int netto = cursor.getInt(nettoWorthID);
        int brutto = cursor.getInt(bruttoWorthID);
        int MwSt = cursor.getInt(MwStID);
        byte[] image = cursor.getBlob(imageID);
        int type = cursor.getInt(typeID);
        int accountID = cursor.getInt(accountidID);

        Receipt receipt = new Receipt(dateAdded, netto, brutto, MwSt, image, accountID, type);
        receipt.setId(id);
        return receipt;
    }



    //TRANSACTION_DATENBANKFUNKTIONEN (CREATE, READ, UPDATE, DELETE)

    public Transaction createTransaction(int accountID, String name, int amountOfMoneyInCents, int type) {

        Transaction transaction = new Transaction(accountID, name, amountOfMoneyInCents, type);
        ContentValues values = transaction.getContentValues();

        long insertID = insert(DBHelper.TABLE_TRANSACTION, values);

        transaction = getTransaction((int) insertID);

        return transaction;
    }

    public Transaction getTransaction(int transactionID) {
        Cursor cursor = query(DBHelper.TABLE_TRANSACTION, DBHelper.COLUMNS_TRANSACTION, DBHelper.TRANSACTION_ID, transactionID);
        cursor.moveToFirst();
        Transaction transaction = cursorToTransaction(cursor);
        cursor.close();
        return transaction;
    }

    public List<Transaction> getAllTransactions(int accountID) {

        List<Transaction> transactionList = new ArrayList<>();

        Cursor cursor = query(DBHelper.TABLE_TRANSACTION, DBHelper.COLUMNS_TRANSACTION, DBHelper.TRANSACTION_ACCOUNT_ID, accountID);

        Transaction tmp;

        for (int count = 0; count < cursor.getCount(); count++) {
            cursor.moveToPosition(count);
            tmp = cursorToTransaction(cursor);
            transactionList.add(tmp);
        }

        cursor.close();

        return transactionList;

    }

    private Transaction cursorToTransaction(Cursor cursor) {

        int idID = cursor.getColumnIndex(DBHelper.TRANSACTION_ID);
        int nameID = cursor.getColumnIndex(DBHelper.TRANSACTION_NAME);
        int accountidID = cursor.getColumnIndex(DBHelper.TRANSACTION_ACCOUNT_ID);
        int amountOfMoneyID = cursor.getColumnIndex(DBHelper.TRANSACTION_AMOUNT_OF_MONEY);
        int typeID = cursor.getColumnIndex(DBHelper.TRANSACTION_TYPE);

        int id = cursor.getInt(idID);
        String name = cursor.getString(nameID);
        int accountID = cursor.getInt(accountidID);
        int amountOfMoney = cursor.getInt(amountOfMoneyID);
        int type = cursor.getInt(typeID);

        Transaction transaction = new Transaction(accountID, name, amountOfMoney, type);
        transaction.setId(id);

        return transaction;

    }

    public int deleteTransaction(int transactionID) {
        int resultCode = database.delete(DBHelper.TABLE_TRANSACTION,
                DBHelper.TRANSACTION_ID + " = " + transactionID,
                null);
        return resultCode;

    }

    public int changeTransaction(int transactionID, String name, int amountOfMoneyInCents) {

        Cursor cursor = query(DBHelper.TABLE_TRANSACTION, DBHelper.COLUMNS_TRANSACTION, DBHelper.TRANSACTION_ID, transactionID);

        Transaction newTransaction = cursorToTransaction(cursor);
        newTransaction.setName(name);
        newTransaction.setAmountOfMoneyInCents(amountOfMoneyInCents);
        ContentValues values = newTransaction.getContentValues();

        int resultCode = database.update(DBHelper.TABLE_TRANSACTION, values, "id = ?", new String[]{String.valueOf(transactionID)});
        return resultCode;

    }

}
