package com.example.dima.finanzmanager.NotificationClasses;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.MainActivity;
import com.example.dima.finanzmanager.NavigatorActivity;
import com.example.dima.finanzmanager.R;
import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Receipt;
import com.example.dima.finanzmanager.classes.Transaction;

import java.util.List;

/**
 * Created by Dima on 23.12.2017.
 */

public class AlarmReceiver extends BroadcastReceiver {

    public static final String LOG_TAG = AlarmReceiver.class.getSimpleName();

    private static int accountID;
    private DataSource src;
    private Account account;
    private List<Receipt> receiptList;


    @Override
    @TargetApi(16)
    public void onReceive(Context context, Intent intent) {

             accountID = intent.getExtras().getInt(DBHelper.ACCOUNT_ID);
             setDataFromDB(context);

             Log.d(LOG_TAG, "AccountID AlarmReceiver.class: " + accountID);

             NotificationHandler.setTimedNotificationTime();
             new NotificationHandler().createTimedNotification(context, accountID);

            if (checkForUnfilledReceipts() && account != null) {
                Intent intent1 = new Intent(context, NavigatorActivity.class);
                intent1.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                intent1.putExtra("StartScreen", R.id.nav_accounts);
                PendingIntent pending = PendingIntent.getActivity(context,
                        accountID,
                        intent1,
                        PendingIntent.FLAG_ONE_SHOT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentTitle(account.getName())
                        .setSmallIcon(R.drawable.ic_euro_notification)
                        .setContentText("Bitte Daten ergänzen.")
                        .setContentIntent(pending)
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

                NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mgr.notify(accountID, builder.build());
            }
    }

    public void setDataFromDB(Context context) {
        src = new DataSource(context);
        src.open();
        account = src.getAccount(accountID);
        receiptList = src.getAllReceipts(accountID);
        Log.d(LOG_TAG, "AlarmReceiver.class: " + (account == null) + " " + receiptList.size());
        src.close();
    }

    public boolean checkForUnfilledReceipts() {
        if (receiptList.size() > 0) {
            Receipt tmp;
            for (int count = 0; count < receiptList.size(); count++) {
                tmp = receiptList.get(count);
                if (tmp.getBruttoInCents() == 0)
                    return true;
            }
        }
        return false;
    }

}
