package com.example.dima.finanzmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.classes.Account;

import java.util.List;

/**
 * Created by Dima on 28.12.2017.
 */

public class PopUpAccount extends AppCompatActivity {

    public static final String LOG_TAG = PopUpAccount.class.getSimpleName();

    //All Views.
    private Button add;
    private Button cancel;
    private TextInputEditText name;
    private TextInputEditText balance;

    //Datenbankschnittstelle.
    private DataSource src;

    private NotificationHandler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new NotificationHandler();
        src = new DataSource(this);


        setContentView(R.layout.popup_account);

        setViews();
        changeToPopupWindow();

    }

    public void setViews() {
        name = (TextInputEditText) findViewById(R.id.nameAccount);
        balance = (TextInputEditText) findViewById(R.id.balanceAccount);
        add = (Button) findViewById(R.id.add_1);
        cancel = (Button) findViewById(R.id.cancel_1);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                src.open();
                String accountName = name.getText().toString();
                String accountBalance = balance.getText().toString();
                List<Account> accounts = src.getAllAccounts();
                if (accountName.equals("") || accountBalance.equals("")) {
                    Toast.makeText(getBaseContext(), "Bitte vollständig ausfüllen.", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int count = 0; count < accounts.size(); count++) {
                    if (accounts.get(count).getName().equals(accountName)) {
                        Toast.makeText(getBaseContext(), "Account mit diesem Namen bereits vorhanden.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                Account createdAccount = src.createAccount(accountName, ((int) (100 * Float.valueOf(accountBalance))));
                handler.createTimedNotification(getBaseContext(), createdAccount.getId());
                handler.createTimedTransactions(getBaseContext(), createdAccount.getId());
                src.close();
                startActivity(new Intent(getBaseContext(), AccountActivity.class));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void changeToPopupWindow() {

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        Window window = getWindow();

        window.setLayout((int) (.9*width), (int) (.6*height));
        window.getAttributes().dimAmount = 0.75f;
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setTitle("Account erstellen");
        setFinishOnTouchOutside(true);

    }


}
