package com.example.dima.finanzmanager.classes;

import android.content.ContentValues;

import com.example.dima.finanzmanager.DBClasses.DBHelper;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dima on 19.12.2017.
 */

public class Receipt {

    private int id;
    private String dateAdded;
    private int nettoInCents;
    private int bruttoInCents;
    private int MwStInCents;
    private byte[] image;
    private int accountID;
    private int type;

    public Receipt(byte[] image, int accountID, int type) {
        this.type = type;
        this.image = image;
        this.accountID = accountID;
        this.dateAdded = getDateTime();

    }


    public Receipt(String dateAdded, int nettoInCents, int bruttoInCents, int MwStInCents, byte[] image, int accountID, int type) {
        this.type = type;
        this.dateAdded = dateAdded;
        this.nettoInCents = nettoInCents;
        this.bruttoInCents = bruttoInCents;
        this.MwStInCents = MwStInCents;
        this.image = image;
        this.accountID = accountID;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
       // values.put("purpose", purpose);
        values.put(DBHelper.RECEIPT_DATE_ADDED, dateAdded);
        values.put(DBHelper.RECEIPT_NETTO, nettoInCents);
        values.put(DBHelper.RECEIPT_BRUTTO, bruttoInCents);
        values.put(DBHelper.RECEIPT_MWST, MwStInCents);
        values.put(DBHelper.RECEIPT_IMAGE, image);
        values.put(DBHelper.RECEIPT_TYPE, type);
        values.put(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
        return values;
    }

    public int getId() {
        return id;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public int getBruttoInCents() {
        return bruttoInCents;
    }

    public int getNettoInCents() {
        return nettoInCents;
    }

    public int getMwStInCents() {
        return MwStInCents;
    }

    public byte[] getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBruttoInCents(int bruttoInCents) {
        this.bruttoInCents = bruttoInCents;
    }

    public void setNettoInCents(int nettoInCents) {
        this.nettoInCents = nettoInCents;
    }

    public void setMwStInCents() {
    }

    public double getBruttoInEuro() {
        double brutto = getBruttoInCents();
        return brutto / 100.0;
    }

    public double getNettoInEuro() {
        double netto = getNettoInCents();
        return netto / 100.0;
    }

    public double getMwStInEuro() {
        double MwSt = getMwStInCents();
        return MwSt / 100.0;
    }

    public String getBruttoInEuroAsString() {
        return NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(getBruttoInEuro());
    }

    public String getNettoInEuroAsString() {
        return NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(getNettoInEuro());
    }

    public String getMwStInEuroAsString() {
        return NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(getBruttoInEuro());
    }

    public int getAccountID() {
        return accountID;
    }

    public int getType() {
        return type;
    }
}
