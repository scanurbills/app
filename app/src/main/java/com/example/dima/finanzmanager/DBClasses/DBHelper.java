package com.example.dima.finanzmanager.DBClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Dima on 14.12.2017.
 * Diese Klasse erzeugt die Datenbank und enthält wichtige Informationen über diese.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = DBHelper.class.getSimpleName();


    public static final String DB_NAME = "Database.db";
    public static final int DB_VERSION = 3;

    public static final String TABLE_ACCOUNT = "Account";
    public static final String TABLE_RECEIPT = "Receipt";
    public static final String TABLE_TRANSACTION = "Proceeding";


    public static final String ACCOUNT_ID = "id";
    public static final String ACCOUNT_NAME = "name";
    public static final String ACCOUNT_BALANCE = "accountBalance";

    public static final String RECEIPT_ID = "id";
    public static final String RECEIPT_DATE_ADDED = "dateAdded";
    public static final String RECEIPT_BRUTTO = "brutto";
    public static final String RECEIPT_NETTO = "netto";
    public static final String RECEIPT_MWST = "MwSt";
    public static final String RECEIPT_IMAGE = "image";
    public static final String RECEIPT_ACCOUNT_ID = "account_id";
    public static final String RECEIPT_TYPE = "type";


    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_NAME = "name";
    public static final String TRANSACTION_AMOUNT_OF_MONEY = "amountOfMoney";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_ACCOUNT_ID = "account_id";


    public static final String CREATE_TABLE_ACCOUNT = "CREATE TABLE " + TABLE_ACCOUNT +
            "( " + ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ACCOUNT_NAME + " VARCHAR(63) NOT NULL, " +
            ACCOUNT_BALANCE + " INTEGER NOT NULL);";

    public static final String CREATE_TABLE_RECEIPT = "CREATE TABLE " + TABLE_RECEIPT +
            "( " + RECEIPT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            RECEIPT_DATE_ADDED + " TEXT NOT NULL, " +
            RECEIPT_BRUTTO + " INTEGER DEFAULT 0, " +
            RECEIPT_NETTO + " INTEGER DEFAULT 0, " +
            RECEIPT_MWST + " INTEGER DEFAULT 0," +
            RECEIPT_IMAGE + " BLOB NOT NULL, " +
            RECEIPT_ACCOUNT_ID + " INTEGER NOT NULL, " +
            RECEIPT_TYPE + " BOOLEAN NOT NULL, " +
            "FOREIGN KEY (" + RECEIPT_ACCOUNT_ID + ") REFERENCES " + TABLE_ACCOUNT + "(" + ACCOUNT_ID + ") ON DELETE CASCADE);";


    public static final String CREATE_TABLE_TRANSACTION = "CREATE TABLE " + TABLE_TRANSACTION +
            "( " + TRANSACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TRANSACTION_NAME + " VARCHAR(90) NOT NULL, " +
            TRANSACTION_AMOUNT_OF_MONEY + " INTEGER NOT NULL, " +
            TRANSACTION_TYPE + " BOOLEAN NOT NULL, " +
            TRANSACTION_ACCOUNT_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + TRANSACTION_ACCOUNT_ID + ") REFERENCES " + TABLE_ACCOUNT + "(" + ACCOUNT_ID + ") ON DELETE CASCADE);";




    public static final String[] COLUMNS_RECEIPT = {"id", "dateAdded", "brutto", "netto", "MwSt", "image", "account_id", "type"};
    public static final String[] COLUMNS_ACCOUNT = {"id", "name", "accountBalance"};
    public static final String[] COLUMNS_TRANSACTION = {"id", "name", "amountOfMoney", "type", "account_id"};



    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        //Wurde keine Datenbank mit dem Bezeichner von 'name' gefunden wird onCreate automatisch aufgerufen um eine neue Datenbank zu erstellen.
        Log.d(LOG_TAG, "Es wurde die Datenbank: " + getDatabaseName() + " erzeugt.");
    }
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_ACCOUNT);
            db.execSQL(CREATE_TABLE_RECEIPT);
            db.execSQL(CREATE_TABLE_TRANSACTION);
            db.execSQL("PRAGMA foreign_keys=ON;");
            Log.d(LOG_TAG, "Die Tabellen wurden angelegt.");
        }
        catch (Exception e){
            Log.e(LOG_TAG, "Fehler beim Anlegen der Tabelle: " +
            e.getMessage());
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIPT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
        onCreate(db);
    }

    @Override
    public void close() {
        super.close();
        Log.d(LOG_TAG, "DBHelper wurde geschlossen.");
    }


}
