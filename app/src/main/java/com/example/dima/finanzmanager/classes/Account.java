package com.example.dima.finanzmanager.classes;

import android.content.ContentValues;

import com.example.dima.finanzmanager.DBClasses.DBHelper;

import java.sql.Date;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Dima on 04/12/2017.
 */

public class Account {

    private int id;
    private String name;
    private int accountBalanceInCents;

    public Account(String name, int accountBalanceInCents) {
        this.name = name;
        this.accountBalanceInCents = accountBalanceInCents;
    }


    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DBHelper.ACCOUNT_NAME, name);
        values.put(DBHelper.ACCOUNT_BALANCE, accountBalanceInCents);
        return values;
    }

    public String getName() {
        return name;
    }

    public int getAccountBalanceInCents() {
        return accountBalanceInCents;
    }

    public double getAccountBalanceInEuro() {
        double balance = accountBalanceInCents / 100.0;
        return balance;
    }

    public String getAccountBalanceInEuroAsString() {
        return NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(getAccountBalanceInEuro());
    }

    public int getId() { return id; }

    public void setName(String name){
        this.name = name;
    }

    public void setAccountBalance(int accountBalanceInCents) {
        this.accountBalanceInCents = accountBalanceInCents;
    }

    public void setId(int id) { this.id = id; }

}
