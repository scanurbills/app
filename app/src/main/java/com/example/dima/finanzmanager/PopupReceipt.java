package com.example.dima.finanzmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;


/**
 * Created by Dima on 17.01.2018.
 */

public class PopupReceipt extends Activity {

    //Daten von NavigatorActivity übergeben.
    private static int receiptID;
    private static byte[] imageByteArray;
    private static int accountID;

    //Alle Views.
    private Button add;
    private Button cancel;
    private ImageView image;

    //Datenbankschnittstelle.
    private DataSource src;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try { receiptID = getIntent().getExtras().getInt(DBHelper.RECEIPT_ID); } catch (Exception e) {}
        try { imageByteArray = getIntent().getExtras().getByteArray(DBHelper.RECEIPT_IMAGE); } catch (Exception e) {}
        try { accountID = getIntent().getExtras().getInt(DBHelper.RECEIPT_ACCOUNT_ID); } catch (Exception e) {}
        src = new DataSource(this);

        setContentView(R.layout.popup_receipt);

        setViews();
        setViewContents();
        changeToPopupWindow();

    }


    public void setViews() {

        add = (Button) findViewById(R.id.add_2);
        cancel = (Button) findViewById(R.id.cancel_2);
        image = (ImageView) findViewById(R.id.edit_receipt_image);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                src.open();

                TextInputEditText brutto = (TextInputEditText) findViewById(R.id.bruttoEditReceipt);

                int bruttoInCents = (int) (Double.valueOf(brutto.getText().toString()) * 100.0);

             if (bruttoInCents == 0)
                {
                    Toast.makeText(getBaseContext(), "Bitte füllen Sie die Daten vollständig aus.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                 Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                 if (src.getReceipt(receiptID).getBruttoInCents() == 0)
                     intent.putExtra("StartScreen", R.id.nav_accounts);
                 else
                     intent.putExtra("StartScreen", R.id.nav_home);
                 intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                 src.changeReceipt(receiptID, bruttoInCents);
                 src.close();
                 startActivity(intent);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageByteArray = new byte[]{0};
                finish();
            }
        });

    }

    public void setViewContents() {

        Bitmap imageBitmap = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
        image.setImageBitmap(imageBitmap);

    }

    public void changeToPopupWindow() {

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        Window window = getWindow();
        window.setLayout((int) (.95*width), (int) (.9*height));
        window.getAttributes().dimAmount = 0.75f;
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setFinishOnTouchOutside(true);

    }

}
