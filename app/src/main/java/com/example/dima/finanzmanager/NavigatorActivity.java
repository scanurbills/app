package com.example.dima.finanzmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;

import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.content.ContentValues;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.AlarmReceiver;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.NotificationClasses.TransactionsReceiver;
import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Receipt;
import com.example.dima.finanzmanager.classes.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dima on 20.01.2018.
 */

public class NavigatorActivity extends AppCompatActivity {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    //All Views.
    private BottomNavigationView navigationView;
    private List<Receipt> unfilledReceipts;
    private ReceiptAdapter unfilledReceiptsAdapter;
    private ListView unfilledReceiptsListView;
    private List<Receipt> filledReceipts;
    private ReceiptAdapter filledReceiptsAdapter;
    private ListView filledReceiptsListView;
    private List<Transaction> transactions;
    private ListView transactionsListView;
    private TransactionAdapter transactionsAdapter;

    private FloatingActionButton createTransactionButton;

    //AccountID vom eingeloggten Account.
    private static int accountID;

    //Datenbankschnittstelle
    private DataSource src;

    //Erstes Ubersichtsfenster das zu sehen sein soll.
    private static int startScreenID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {accountID = getIntent().getExtras().getInt(DBHelper.RECEIPT_ACCOUNT_ID);} catch (Exception e){}
        try {startScreenID = getIntent().getExtras().getInt("StartScreen");} catch (Exception e) {}
        src = new DataSource(this);

        setContentView(R.layout.activity_navigator);

        setViews();
        setLists();
        setNavigationView();
        setListContents();
    }

    public void showAlertDialogDeleteReceipt(final Receipt receiptToDelete) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Beleg Löschen?")
                .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        src.open();
                        src.changeReceipt(receiptToDelete.getId(), 0);
                        src.deleteReceipt(receiptToDelete.getId());
                        List<Receipt> receipts = src.getAllReceipts(receiptToDelete.getId());
                        src.close();
                        filledReceiptsAdapter.notifyDataSetChanged();
                        Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                        intent.putExtra("StartScreen", startScreenID);
                        startActivity(new Intent(getBaseContext(), NavigatorActivity.class));
                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    public void showAlertDialogDeleteTransaction(final Transaction transactionToDelete) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Transaktion löschen?")
                .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        src.open();
                        src.deleteTransaction(transactionToDelete.getId());
                        src.close();
                        filledReceiptsAdapter.notifyDataSetChanged();
                        Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                        intent.putExtra("StartScreen", R.id.nav_transactions);
                        startActivity(new Intent(getBaseContext(), NavigatorActivity.class));
                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();


    }

    public void showAlertDialogCreateTransaction() {

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);


        final TextInputEditText nameEdit = new TextInputEditText(this);
        final TextInputEditText balanceEdit = new TextInputEditText(this);

        nameEdit.setHint("name");
        nameEdit.setSingleLine();

        balanceEdit.setHint("amount");
        balanceEdit.setSingleLine();

        final CheckBox income = new CheckBox(this);
        final CheckBox spending = new CheckBox(this);
        income.setText("Einkommen");
        spending.setText("Ausgabe");

        layout.addView(nameEdit);
        layout.addView(balanceEdit);
        layout.addView(income);
        layout.addView(spending);

        income.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    spending.setChecked(false);
                }
            }
        });

        spending.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    income.setChecked(false);
                }
            }
        });

        layout.setDividerPadding(16);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Transaktion hinzufügen")
                .setView(layout)
                .setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        src.open();

                        if (income.isChecked()) {
                            src.createTransaction(accountID, nameEdit.getText().toString(),
                                    (int) ((Double.valueOf(balanceEdit.getText().toString()) * 100.0)),
                                    1);
                        }
                        else if (spending.isChecked()) {
                            src.createTransaction(accountID, nameEdit.getText().toString(),
                                    (int) ((Double.valueOf(balanceEdit.getText().toString()) * 100.0)),
                                    0);
                        }

                        transactions = src.getAllTransactions(accountID);

                        src.close();

                        transactionsAdapter.notifyDataSetChanged();
                        Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                        intent.putExtra("StartScreen", R.id.nav_transactions);
                        startActivity(new Intent(getBaseContext(), NavigatorActivity.class));

                        }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    public void setNavigationView() {
        navigationView = (BottomNavigationView) findViewById(R.id.navigator);
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_home);
        navigationView.setSelectedItemId(menuItem.getItemId());

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                LinearLayout layoutTransactions = (LinearLayout) findViewById(R.id.LinearLayout);
                LinearLayout layoutFilledReceipts = (LinearLayout) findViewById(R.id.LinearLayout2);
                LinearLayout layoutUnfilledReceipts = (LinearLayout) findViewById(R.id.LinearLayout3);
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startScreenID = R.id.nav_home;
                        layoutTransactions.setVisibility(View.GONE);
                        layoutUnfilledReceipts.setVisibility(View.GONE);
                        createTransactionButton.setVisibility(View.GONE);
                        layoutFilledReceipts.setVisibility(View.VISIBLE);

                        break;
                    case R.id.nav_accounts:
                        startScreenID = R.id.nav_accounts;
                        layoutFilledReceipts.setVisibility(View.GONE);
                        layoutTransactions.setVisibility(View.GONE);
                        createTransactionButton.setVisibility(View.GONE);
                        layoutUnfilledReceipts.setVisibility(View.VISIBLE);
                        break;
                    case R.id.nav_transactions:
                        startScreenID = R.id.nav_transactions;
                        layoutFilledReceipts.setVisibility(View.GONE);
                        layoutUnfilledReceipts.setVisibility(View.GONE);
                        layoutTransactions.setVisibility(View.VISIBLE);
                        createTransactionButton.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        navigationView.setSelectedItemId(startScreenID);

    }

    public void setViews() {

        createTransactionButton = (FloatingActionButton) findViewById(R.id.create_transaction_button);

        createTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialogCreateTransaction();
            }
        });

    }

    public void setLists() {

        filledReceiptsAdapter = new ReceiptAdapter(this, R.layout.receipt_overview_row);
        unfilledReceiptsAdapter = new ReceiptAdapter(this, R.layout.receipt_overview_row);
        transactionsAdapter = new TransactionAdapter(this, R.layout.account_row);
        filledReceiptsListView = (ListView) findViewById(R.id.receipt_list_filled_receipts);
        unfilledReceiptsListView = (ListView) findViewById(R.id.receipt_list_unfilled_receipts);
        transactionsListView = (ListView) findViewById(R.id.transaction_list);


        filledReceiptsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Receipt receiptClicked = (Receipt) filledReceiptsListView.getItemAtPosition(position);
                showAlertDialogDeleteReceipt(receiptClicked);
                return true;
            }
        });

        unfilledReceiptsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Receipt receiptClicked = (Receipt) unfilledReceiptsListView.getItemAtPosition(position);
                showAlertDialogDeleteReceipt(receiptClicked);
                return true;
            }
        });

        transactionsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Transaction transactionClicked = (Transaction) transactionsListView.getItemAtPosition(position);
                showAlertDialogDeleteTransaction(transactionClicked);
                return true;
            }
        });


    }

    public void setListContents() {

        src.open();

        List<Receipt> receipts = src.getAllReceipts(accountID);

        filledReceipts = new ArrayList<>();
        for (int count = 0; count < receipts.size(); count++) {
            if (receipts.get(count).getBruttoInCents() != 0)
                filledReceipts.add(receipts.get(count));
        }

        unfilledReceipts = new ArrayList<>();
        for (int count = 0; count < receipts.size(); count++) {
            if (receipts.get(count).getBruttoInCents() == 0)
                unfilledReceipts.add(receipts.get(count));
        }

        transactions = src.getAllTransactions(accountID);


        transactionsAdapter.addAll(transactions);
        transactionsListView.setAdapter(transactionsAdapter);
        filledReceiptsAdapter.addAll(filledReceipts);
        filledReceiptsListView.setAdapter(filledReceiptsAdapter);
        unfilledReceiptsAdapter.addAll(unfilledReceipts);
        unfilledReceiptsListView.setAdapter(unfilledReceiptsAdapter);

        src.close();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ScanOrOverview.class);
        intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
        startActivity(intent);
    }

}
