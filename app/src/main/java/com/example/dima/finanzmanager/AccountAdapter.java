package com.example.dima.finanzmanager;

import android.content.ClipData;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.classes.Account;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dima on 08.01.2018.
 */

public class AccountAdapter extends ArrayAdapter<Account> {


    public AccountAdapter(Context context, int resource) {
        super(context, resource);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.account_row, null);
        }

        Account account = getItem(position);

        TextView name = (TextView) v.findViewById(R.id.nameAccountRow);
        TextView balance = (TextView) v.findViewById(R.id.balanceAccountRow);
        name.setText(account.getName());
        balance.setText(account.getAccountBalanceInEuroAsString());

        return v;

    }
}
