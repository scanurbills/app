package com.example.dima.finanzmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Receipt;
import com.example.dima.finanzmanager.classes.Transaction;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dima on 18.01.2018.
 */

public class ScanOrOverview extends AppCompatActivity {

    //Daten von anderen Activities übergeben.
    private static Account account;
    private static Receipt lastReceipt;
    private static int accountID;

    //Alle Views.
    private FloatingActionButton overviewButton;
    private Button incomeButton;
    private Button spendButton;
    private TextView accountBalanceView;
    private TextView accountEarnings;
    private TextView accountSpendings;
    private TextView countUnfilledReceipts;
    private ListView unfilledReceipt;
    private ReceiptAdapter unfilledReceiptAdapter;


    //Datenbankschnittstelle
    private DataSource src;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try { accountID = getIntent().getExtras().getInt(DBHelper.RECEIPT_ACCOUNT_ID); } catch (Exception e) {}
        src = new DataSource(this);



        setContentView(R.layout.scan_or_overview);
        setViews();
        setViewContents();

    }

    public void setViews() {

        overviewButton = (FloatingActionButton) findViewById(R.id.receipt_overview_button);
        incomeButton = (Button) findViewById(R.id.incoming);
        spendButton = (Button) findViewById(R.id.spending);
        accountBalanceView = (TextView) findViewById(R.id.scan_or_overview_balance);
        accountEarnings = (TextView) findViewById(R.id.scan_or_overview_earnings);
        accountSpendings = (TextView) findViewById(R.id.scan_or_overview_spendings);
        unfilledReceipt = (ListView) findViewById(R.id.lastReceipt);

        incomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), OCR.class);
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                intent.putExtra(DBHelper.RECEIPT_TYPE, 1);
                startActivity(intent);
            }
        });

        spendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), OCR.class);
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                intent.putExtra(DBHelper.RECEIPT_TYPE, 0);
                startActivity(intent);
            }
        });


        overviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                intent.putExtra("StartScreen", R.id.nav_accounts);
                startActivity(intent);
            }
        });

    }

    public void setViewContents() {
        src.open();
        account = src.getAccount(accountID);
        accountBalanceView.setText(account.getAccountBalanceInEuroAsString());

        List<Receipt> receiptList = src.getAllReceipts(accountID);
        if (receiptList.size() > 0) {
            lastReceipt = receiptList.get(receiptList.size()-1);
            unfilledReceiptAdapter = new ReceiptAdapter(getBaseContext(), R.layout.receipt_overview_row);
            unfilledReceiptAdapter.add(lastReceipt);
            unfilledReceipt.setAdapter(unfilledReceiptAdapter);
        }

        int earnings = 0;
        int spendings = 0;

        for (int count = 0; count < receiptList.size(); count++) {
            if (receiptList.get(count).getType() == 0)
                spendings -= receiptList.get(count).getBruttoInCents();
            else if (receiptList.get(count).getType() == 1) {
                earnings += receiptList.get(count).getBruttoInCents();
            }
        }


        double earningsInEuro = ((double) earnings) / 100.0;
        double spendingsInEuro = ((double) spendings) / 100.0;
        accountEarnings.setText( NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(earningsInEuro));
        accountSpendings.setText( NumberFormat.getCurrencyInstance(new Locale("de", "DE")).format(spendingsInEuro));

        src.close();

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, AccountActivity.class));
    }
}
