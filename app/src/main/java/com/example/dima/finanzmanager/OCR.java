package com.example.dima.finanzmanager;

/**
 * Created by julez on 22.11.2017.
 */
        import android.Manifest;
        import android.annotation.TargetApi;
        import android.app.Activity;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Matrix;
        import android.media.ExifInterface;
        import android.net.Uri;
        import android.os.Bundle;
        import android.os.Environment;
        import android.os.StrictMode;
        import android.provider.MediaStore;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.content.ContextCompat;
        import android.support.v7.app.AppCompatActivity;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Toast;
        import android.widget.TextView;

        import com.example.dima.finanzmanager.DBClasses.DBHelper;
        import com.example.dima.finanzmanager.DBClasses.DataSource;
        import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
        import com.example.dima.finanzmanager.classes.Receipt;
        import com.googlecode.tesseract.android.TessBaseAPI;

        import java.io.ByteArrayOutputStream;
        import java.io.File;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;

public class OCR extends AppCompatActivity {

    private static final String LOG_TAG = OCR.class.getSimpleName();

    //Eingeloggter Account Daten.
    private static int receiptType;
    private static int accountID;


    //TESSARACT DATA
    public static final String TESS_DATA = "/tessdata";
    private static final String DATA_PATH = Environment.getExternalStorageDirectory().toString();
    private TessBaseAPI tessBaseAPI;
    private Uri outputFileDir;
    final int CAMERA_CAPTURE = 1;
    final int CROP_PIC = 2;


    //Views
    private EditText editBrutto;
    private ImageView receiptImage;
    private Button addButton;
    private Button scanAgainButton;
    private Button editLaterButton;

    //Datenbankschnittstelle und erzeugter Beleg.
    private DataSource src;
    private Receipt createdReceipt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //file exposure bug fix
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        try { receiptType = getIntent().getExtras().getInt(DBHelper.RECEIPT_TYPE); } catch (Exception e) {}
        try { accountID = getIntent().getExtras().getInt(DBHelper.RECEIPT_ACCOUNT_ID); } catch (Exception e) {}
        src = new DataSource(this);

        setContentView(R.layout.activity_ocr);

        setViews();
        checkPermission();
        startCameraActivity();

    }

    public void setViews() {

        editBrutto = (EditText) this.findViewById(R.id.bruttoEditScan);
        receiptImage = (ImageView) findViewById(R.id.receipt_image_scan);
        addButton = (Button) findViewById(R.id.add_3);
        scanAgainButton = (Button) findViewById(R.id.scan_again);
        editLaterButton = (Button) findViewById(R.id.edit_later);
        scanAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                src.open();
                src.deleteReceipt(createdReceipt.getId());
                src.close();
                checkPermission();
                startCameraActivity();
            }
        });

        editLaterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NotificationHandler().createTimedNotification(getBaseContext(), accountID);
                Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                intent.putExtra("StartScreen", R.id.nav_accounts);
                startActivity(intent);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int bruttoInCents = (int) (Double.valueOf(editBrutto.getText().toString())*100.0);
                if (bruttoInCents == 0)
                {
                    Toast.makeText(getBaseContext(), "Bitte füllen Sie die Daten vollständig aus.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    src.open();
                    if (createdReceipt != null) {
                        createdReceipt.setBruttoInCents((int) (Double.valueOf(editBrutto.getText().toString()) * 100.0));
                        createdReceipt.setMwStInCents();
                        src.changeReceipt(createdReceipt.getId(), createdReceipt.getBruttoInCents());
                    }
                    src.close();
                    Intent intent = new Intent(getBaseContext(), NavigatorActivity.class);
                    intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountID);
                    intent.putExtra("StartScreen", R.id.nav_home);
                    startActivity(intent);
                }
            }
        });



    }

    public void setImageViewContentAndSaveDB(Uri imageUri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 7;
        Bitmap bitmap = BitmapFactory.decodeFile(imageUri.getPath(),options);
        receiptImage.setImageBitmap(bitmap);

        //Receipt wir erzeugt und in Datenbank gespeichert.
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageAsByteArray = stream.toByteArray();

        src.open();
        createdReceipt = src.createReceipt(accountID, imageAsByteArray, receiptType);
        src.close();

    }

    @TargetApi(21)
    private void startOCR(Bitmap bitmap){
        Log.d(LOG_TAG, "startOcr");

        try {

            //OCR mit geschnittenen Bild starten
            String result = this.getText(bitmap);
            editBrutto.setText(result);
        }
        catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    private void fixOrientation(Bitmap bitmap, Uri path){
    //Orientation korrigieren
        try {
        ExifInterface exif = new ExifInterface(path.getPath());
        int exifOrientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        Log.v(LOG_TAG, "Orient: " + exifOrientation);

        int rotate = 0;

        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
        }

        Log.v(LOG_TAG, "Rotation: " + rotate);

        if (rotate != 0) {

            //  Werte vom gegebenen Image
            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            // Setze pre rotate Matrix
            Matrix mtx = new Matrix();
            mtx.preRotate(rotate);

            // Rotate Bitmap
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
        }

        // ARGB_8888 für OCR
            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            startOCR(bitmap);

    } catch (IOException e) {
        Log.e(LOG_TAG, "Konnte Orientation nicht korrigieren: " + e.toString());
    }}



    private void checkPermission() {
        Log.d(LOG_TAG, "checkPermission start");
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 120);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 121);
        }
    }

    private void startCameraActivity(){
        Log.d(LOG_TAG, "startCameraActivity");
        try{
            String imagePath = DATA_PATH;
            File dir = new File(imagePath);
            if(!dir.exists())  dir.mkdir();

            String imageFilePath = imagePath+"/ocr_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
            outputFileDir = Uri.fromFile(new File(imageFilePath));

            Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileDir);
            if(pictureIntent.resolveActivity(getPackageManager() ) != null){
                startActivityForResult(pictureIntent, CAMERA_CAPTURE);
            }
        } catch (Exception e){
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(LOG_TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CROP_PIC) {
            if (resultCode == Activity.RESULT_OK) {

                Bundle extras = data.getExtras();
                Bitmap croppedPic = extras.getParcelable("data");
                prepareTessData();
                fixOrientation(croppedPic, outputFileDir);
                setImageViewContentAndSaveDB(outputFileDir);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Result canceled.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, ScanOrOverview.class));
            } else {
                Toast.makeText(getApplicationContext(), "Activity result failed.", Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode == CAMERA_CAPTURE){
            performCrop();
        }
    }

    private void performCrop() {
        Log.d(LOG_TAG, "performCrop");
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(outputFileDir, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 2);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 256);
        cropIntent.putExtra("outputY", 256);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, CROP_PIC);


    }


    private void prepareTessData(){
        Log.d(LOG_TAG, "prepareTessData");
        try{
            File dir = new File(DATA_PATH + TESS_DATA);
            if(!dir.exists()){
                dir.mkdir();
            }
            String fileList[] = getAssets().list("");
            for(String fileName : fileList){
                String pathToDataFile = DATA_PATH+TESS_DATA+"/"+fileName;
                if(!(new File(pathToDataFile)).exists()){
                    InputStream in = getAssets().open(fileName);
                    OutputStream out = new FileOutputStream(pathToDataFile);
                    byte [] buff = new byte[1024];
                    int len ;
                    while(( len = in.read(buff)) > 0){
                        out.write(buff,0,len);
                    }
                    in.close();
                    out.close();
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage() + " not found");
        }
    }

    private String getText(Bitmap bitmap){
        Log.d(LOG_TAG, "getText");
        try{
            tessBaseAPI = new TessBaseAPI();
        }catch (Exception e){
            Log.e(LOG_TAG, e.getMessage());
        }

        //Tesseract Konfiguration
        tessBaseAPI.init(DATA_PATH,"eng+eng1");
        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
        tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "!?@#$%&*()<>_-+=/:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, ".,0123456789");
        tessBaseAPI.setVariable("classify_bln_numeric_mode", "1");
        tessBaseAPI.setImage(bitmap);


        String retStr = "";
        try{
            retStr = tessBaseAPI.getUTF8Text();
        }catch (Exception e){
            Log.e(LOG_TAG, e.getMessage());
        }
        tessBaseAPI.end();
        return retStr;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(LOG_TAG, "onRequestPermissionsResult");
        switch (requestCode){
            case 120:{
                if(grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Read permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case 121:{
                if(grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Write permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ScanOrOverview.class));
    }


}
