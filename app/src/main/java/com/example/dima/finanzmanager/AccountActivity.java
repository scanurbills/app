package com.example.dima.finanzmanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.AlarmReceiver;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.NotificationClasses.TransactionsReceiver;
import com.example.dima.finanzmanager.classes.Account;

import java.util.List;

/**
 * Created by Dima on 07.01.2018.
 */

public class AccountActivity extends AppCompatActivity {


    public static final String LOG_TAG = AccountActivity.class.getSimpleName();

    //Alle Views.
    private BottomNavigationView navigationView;
    private MenuItem menuItem;
    private ListView accountListView;
    private AccountAdapter accountListAdapter;
    private List<Account> accountList;
    private FloatingActionButton addButton;
    private FloatingActionButton settingsButton;

    //Datenbankschnittstelle
    private DataSource src;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_list_activity);

        setViews();
        setListContents();

    }


    private void setViews() {

        accountListAdapter = new AccountAdapter(this, R.layout.account_row);
        accountListView = (ListView) findViewById(R.id.list_view);
        addButton = (FloatingActionButton) findViewById(R.id.add_button);
        settingsButton = (FloatingActionButton) findViewById(R.id.settingsButton);
        src = new DataSource(this);

        accountListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), ScanOrOverview.class);
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, accountList.get(position).getId());
                startActivity(intent);
            }
        });

        accountListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final Account selectedAccount = (Account) accountListView.getItemAtPosition(position);

                PopupMenu popup = new PopupMenu(getBaseContext(), view);
                popup.getMenuInflater().inflate(R.menu.account_settings_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete:
                                showAlertDialogDeleteAccount(selectedAccount);
                                return true;
                            case R.id.rename:
                                showAlertDialogRenameAccount(selectedAccount);
                                return true;
                        }
                        return true;
                    }
                });

                popup.show();

                return true;
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), PopUpAccount.class));
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), SettingsActivity.class));
            }
        });


    }

    private void setListContents() {
        src.open();
        accountList = src.getAllAccounts();
        accountListAdapter.addAll(accountList);
        accountListView.setAdapter(accountListAdapter);
        src.close();
    }

    private void setSupportItems() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        navigationView = (BottomNavigationView) findViewById(R.id.navigator);
        menuItem = navigationView.getMenu().findItem(R.id.nav_accounts);

        navigationView.setSelectedItemId(menuItem.getItemId());
    }

    private void showAlertDialogDeleteAccount(final Account selectedAccount) {

        final TextInputEditText pinEingabe = new TextInputEditText(this);

        final int PIN = (int) (Math.random()*9999);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Account Löschen")
                .setView(pinEingabe)
                .setMessage("Bitte geben sie diese PIN ein um den Account zu löschen:\n" + String.valueOf(PIN))
                .setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Float.valueOf(pinEingabe.getEditableText().toString()) == PIN) {
                            src.open();
                            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                            Intent intent2 = new Intent(getBaseContext(), TransactionsReceiver.class);
                            AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(ALARM_SERVICE);
                            mgr.cancel(PendingIntent.getBroadcast(getBaseContext(),
                                    selectedAccount.getId(),
                                    intent,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                            mgr.cancel(PendingIntent.getBroadcast(getBaseContext(),
                                    selectedAccount.getId(),
                                    intent2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                            src.deleteAccount(selectedAccount.getId());
                            accountList = src.getAllAccounts();
                            src.close();
                            accountListAdapter.notifyDataSetChanged();
                        }
                        startActivity(new Intent(getBaseContext(), MainActivity.class));
                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    private void showAlertDialogRenameAccount(final Account selectedAccount) {

        final TextInputEditText input = new TextInputEditText(this);


        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Rename")
                .setView(input)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = input.getEditableText().toString();
                        src.changeAccount(selectedAccount.getId(), selectedAccount.getContentValues());
                        accountList = src.getAllAccounts();
                        accountListAdapter.notifyDataSetChanged();
                        startActivity(new Intent(getBaseContext(), MainActivity.class));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    @Override
    public void onBackPressed() {
    }

}
