package com.example.dima.finanzmanager.NotificationClasses;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.classes.Account;

import java.util.List;

/**
 * Created by Dima on 30.01.2018.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            DataSource src = new DataSource(context);
            src.open();
            List<Account> accountList = src.getAllAccounts();
            src.close();
            NotificationHandler handler = new NotificationHandler();
            if (accountList.size() > 0) {
                for (int count = 0; count < accountList.size(); count++) {
                    handler.createTimedNotification(context, accountList.get(count).getId());
                    handler.createTimedTransactions(context, accountList.get(count).getId());
                }
            }
        }

    }

}
