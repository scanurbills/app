package com.example.dima.finanzmanager;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.classes.Account;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1500;

    private DataSource src;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);



        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent homeIntent = new Intent(getBaseContext(), MainActivity.class);
                src = new DataSource(getBaseContext());
                src.open();
                List<Account> accountList = src.getAllAccounts();
                src.close();
                NotificationHandler handler = new NotificationHandler();
                if (accountList.size() > 0) {
                    for (int count = 0; count < accountList.size(); count++) {
                        handler.createTimedNotification(getBaseContext(), accountList.get(count).getId());
                        handler.createTimedTransactions(getBaseContext(), accountList.get(count).getId());
                    }
                }
                startActivity(homeIntent);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
