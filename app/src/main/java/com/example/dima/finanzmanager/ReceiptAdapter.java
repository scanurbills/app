package com.example.dima.finanzmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.classes.Account;
import com.example.dima.finanzmanager.classes.Receipt;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dima on 17.01.2018.
 */

public class ReceiptAdapter extends ArrayAdapter<Receipt> {

    public static final String LOG_TAG = ReceiptAdapter.class.getSimpleName();

    private Receipt receipt;

    private TextView brutto;
    private TextView dateAdded;
    private Button editButton;
    private ImageView image;


    public ReceiptAdapter(Context context, int resource) {
        super(context, resource);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        LayoutInflater vi = LayoutInflater.from(getContext());
        convertView = vi.inflate(R.layout.receipt_overview_row, null);

        receipt = getItem(position);

        setView(convertView);
        setViewColor(convertView);
        setViewContents(convertView);

        return convertView;
    }

    public void setView(View view) {

        brutto = (TextView) view.findViewById(R.id.brutto_overview_row);
        dateAdded = (TextView) view.findViewById(R.id.date_overview_row);
        editButton = (Button) view.findViewById(R.id.edit_receipt_button);
        image = (ImageView) view.findViewById(R.id.receipt_overview_image);

        editButton.setTag(receipt);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Receipt receipt = (Receipt) v.getTag();
                Intent intent = new Intent(v.getContext(), PopupReceipt.class);
                intent.putExtra(DBHelper.RECEIPT_ID, receipt.getId());
                Log.d(LOG_TAG, "receipt ID: " + receipt.getId());
                intent.putExtra(DBHelper.RECEIPT_IMAGE, receipt.getImage());
                intent.putExtra(DBHelper.RECEIPT_ACCOUNT_ID, receipt.getAccountID());
                v.getContext().startActivity(intent);

            }
        });

    }

    public void setViewColor(View view) {
        if (receipt.getType() == 1)
            view.setBackgroundColor(0xff99cc00);
        else
            view.setBackgroundColor(0xffff4444);
    }

    public void setViewContents(View view) {
        brutto.setText(receipt.getBruttoInEuroAsString());
        dateAdded.setText(String.valueOf(receipt.getDateAdded()));
        image.setImageBitmap(BitmapFactory.decodeByteArray(receipt.getImage(), 0, receipt.getImage().length));
    }



}
