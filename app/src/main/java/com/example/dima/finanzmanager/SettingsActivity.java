package com.example.dima.finanzmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.MenuPopupWindow;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.dima.finanzmanager.DBClasses.DBHelper;
import com.example.dima.finanzmanager.DBClasses.DataSource;
import com.example.dima.finanzmanager.NotificationClasses.NotificationHandler;
import com.example.dima.finanzmanager.classes.Account;

import java.util.List;

/**
 * Created by Dima on 25.12.2017.
 */

public class SettingsActivity extends Activity {

    private ListView settingsListView;
    private ArrayAdapter<String> adapter;
    private String[] settings;
    private Resources res;
    private static int checkedAlarmTimeID;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setViews();
        setViewContents();


    }

    public void setViews() {

        settingsListView = (ListView) findViewById(R.id.settings);


        settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        showAlertDialogDeleteAll();
                        break;
                    case 1:
                        //showPopMenuSetAlarmTime(view);
                        break;
                }
            }
        });

    }

    public void setViewContents() {

        res = getResources();
        settings = res.getStringArray(R.array.settingsArray);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, settings);
        settingsListView.setAdapter(adapter);

    }

    public void showAlertDialogDeleteAll() {
        final TextInputEditText pinEingabe = new TextInputEditText(this);

        final int PIN = (int) (Math.random()*999999);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Datenbank löschen")
                .setView(pinEingabe)
                .setMessage("Bitte geben sie diese PIN ein um den Account zu löschen:\n" + String.valueOf(PIN))
                .setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            int zahl = Integer.valueOf(pinEingabe.getText().toString());
                            if (zahl != PIN) {
                                Toast.makeText(getBaseContext(), "PIN wurde falsch eingegeben.", Toast.LENGTH_SHORT).show();
                                return;
                            } else {
                                deleteDatabase(DBHelper.DB_NAME);
                                startActivity(new Intent(getBaseContext(), AccountActivity.class));
                            }
                        } catch (Exception e) {
                            Toast.makeText(getBaseContext(), "Bitte eine Zahl eingeben.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.show();
    }

 /*   public void showPopMenuSetAlarmTime(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.set_alarmtime, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                NotificationHandler handler = new NotificationHandler();
                switch (item.getItemId())
                {
                    case R.id.one_hour:
                        checkedAlarmTimeID = R.id.one_hour;
                        return setAlarmTime(handler, 3600, item);
                    case R.id.six_hours:
                        checkedAlarmTimeID = R.id.six_hours;
                        return setAlarmTime(handler, 7200, item);
                    case R.id.twelve_hours:
                        checkedAlarmTimeID = R.id.twelve_hours;
                        return setAlarmTime(handler, 14400, item);
                    case R.id.one_day:
                        checkedAlarmTimeID = R.id.one_day;
                        return setAlarmTime(handler, 28800, item);
                }
                return true;
            }
        });

        popup.getMenu().getItem(checkedAlarmTimeID).setChecked(false);
        popup.getMenu().getItem(checkedAlarmTimeID).setChecked(true);

        popup.show();
    }

    public boolean setAlarmTime(NotificationHandler handler, int timeInSeconds, MenuItem item) {

        if (item.isChecked()) {
            item.setChecked(false);
        }
        else {
            item.setChecked(true);
        }
        return true;
    }*/

}
